use gloo::events::EventListener;
use minesweeper::core::*;
use minesweeper::interface::*;
use wasm_bindgen::prelude::*;
use web_sys::Document;

use std::cell::RefCell;

thread_local!(static GAME: RefCell<Option<GameHandle>> = RefCell::new(None));
thread_local!(static ON_ENDED: RefCell<Option<js_sys::Function>> = RefCell::new(None));
thread_local!(static ON_STARTED: RefCell<Option<js_sys::Function>> = RefCell::new(None));
thread_local!(static LISTENERS: RefCell<Vec<EventListener>> = RefCell::new(Vec::new()));

const MINE: &str = "💣";
const FLAG: &str = "🚩";
const START: &str = "🙂";
const TENSE: &str = "😬";
const ALIVE: &str = "😅";
const DEAD: &str = "💀";
const WON: &str = "😎";

#[wasm_bindgen]
extern "C" {
    #[wasm_bindgen(js_namespace = console)]
    fn log(s: &str);
}

#[wasm_bindgen]
pub fn init_panic_hook() {
    console_error_panic_hook::set_once();
}

#[wasm_bindgen]
pub fn start_minesweeper(game_started: &js_sys::Function, game_ended: &js_sys::Function) {
    ON_STARTED.with(|cell| *cell.borrow_mut() = Some(game_started.to_owned()));
    ON_ENDED.with(|cell| *cell.borrow_mut() = Some(game_ended.to_owned()));

    let mut level = Difficulty::Beginner;
    GAME.with(|cell| {
        if let Some(game) = &*cell.borrow() {
            level = game.get_difficulty();
        }
    });
    start_minesweeper_with_difficulty(level);
}

fn start_minesweeper_with_difficulty(level: Difficulty) {
    // drop previously registered event listeners to avoid memory leak
    LISTENERS.with(|c| c.borrow_mut().clear());

    let (width, height, mines) = get_params_for_difficulty(level);

    let window = web_sys::window().expect("no global `window` exists");
    let document = window.document().expect("should have a document on window");

    let mut board = String::new();
    board.push_str("<table>");
    for y in 0..height {
        board.push_str("<tr>");
        for x in 0..width {
            board.push_str(&format!(
                "<td><button id=\"{}\" class=\"tile\"></button></td>",
                y * width + x
            ));
        }
        board.push_str("</tr>");
    }
    board.push_str("</table>");

    if let Some(e) = document.get_element_by_id("board") {
        e.set_inner_html(&board);
    }

    for y in 0..height {
        for x in 0..width {
            let i = y * width + x;
            if let Some(tile) = document.get_element_by_id(&format!("{}", i)) {
                let doc = document.clone();
                let mouse_down_listener =
                    EventListener::new(&tile, "mousedown", move |_| set_state_icon(TENSE, &doc));

                let doc = document.clone();
                let mouse_up_listener =
                    EventListener::new(&tile, "mouseup", move |_| set_state_icon(ALIVE, &doc));

                let click_listener =
                    EventListener::new(&tile, "click", move |_| uncover(x, y, width));

                let doc = document.clone();
                let menu_listener =
                    EventListener::new(&tile, "contextmenu", move |_| mark(x, y, width, &doc));

                // remember event listeners so we can drop them when restarting the game
                LISTENERS.with(|c| {
                    c.borrow_mut().push(click_listener);
                    c.borrow_mut().push(menu_listener);
                    c.borrow_mut().push(mouse_down_listener);
                    c.borrow_mut().push(mouse_up_listener);
                });
            }
        }
    }

    if let Some(counter) = document.get_element_by_id("counter") {
        counter.set_inner_html(&format_mine_count(mines));
    }

    let handle = GameHandle::new(level);
    GAME.with(|cell| {
        let mut game = cell.borrow_mut();
        eval_game_state(&handle);
        *game = Some(handle);
    });
}

fn uncover(x: usize, y: usize, width: usize) {
    GAME.with(|cell| match cell.borrow_mut().as_mut() {
        Some(game) => {
            if finished(game.get_game_state()) {
                return;
            }

            let update = game.uncover(x, y);

            let window = web_sys::window().expect("no global `window` exists");
            let document = window.document().expect("should have a document on window");

            for tile in update {
                apply_update(&tile, width, &document);
            }

            eval_game_state(&game);
        }
        None => log("Game not started yet!"),
    });
}

fn mark(x: usize, y: usize, width: usize, document: &Document) {
    GAME.with(|cell| match cell.borrow_mut().as_mut() {
        Some(game) => {
            let update = game.toggle_flag(x, y);
            update_counter(&game, &document);
            apply_update(&update, width, document)
        }
        None => log("Game not started yet!"),
    });
}

fn apply_update(tile: &TileUpdate, width: usize, document: &Document) {
    let i = width * tile.get_y() + tile.get_x();
    let id = format!("{}", i);
    if let Some(e) = document.get_element_by_id(&id) {
        match tile.get_state() {
            TileState::Covered => e.set_inner_html(""),
            TileState::Uncovered(n) => {
                if n == 0 {
                    e.set_attribute("disabled", "true").unwrap();
                    e.set_inner_html("");
                } else {
                    e.set_inner_html(&format!("{}", n));
                    e.set_attribute(&format!("_{}", n), "true").unwrap();
                }
                e.set_attribute("uncovered", "true").unwrap();
            }
            TileState::Detonated => {
                e.set_inner_html(MINE);
                e.set_attribute("detonated", "true").unwrap();
                e.set_attribute("uncovered", "true").unwrap();
            }
            TileState::Marked => {
                e.set_inner_html(FLAG);
                e.set_attribute("marked", "true").unwrap();
            }
            TileState::NoOp => {}
        }
    } else {
        log(&format!("No button with id '{}' found.", id));
    }
}

fn eval_game_state(game: &GameHandle) {
    let window = web_sys::window().expect("no global `window` exists");
    let document = window.document().expect("should have a document on window");

    match game.get_game_state() {
        GameState::NotStarted => {
            set_state_icon(START, &document);
        }
        GameState::Started => {
            report_game_start();
            set_state_icon(ALIVE, &document);
        }
        GameState::Won => {
            report_game_end(true);
            set_state_icon(WON, &document);
        }
        GameState::Lost => {
            report_game_end(false);
            reveal_all_mines(game);
            set_state_icon(DEAD, &document);
        }
    }
    update_counter(&game, &document);
}

fn report_game_start() {
    ON_STARTED.with(|cell| {
        let mut opt = cell.borrow_mut();
        if let Some(fun) = opt.as_ref() {
            let this = JsValue::NULL;
            let x = JsValue::NULL;
            fun.call1(&this, &x)
                .expect("failed to report game start to JS");
            *opt = None;
        }
    });
}

fn report_game_end(won: bool) {
    // unregister button event listener
    LISTENERS.with(|c| c.borrow_mut().clear());
    ON_ENDED.with(|cell| {
        let mut opt = cell.borrow_mut();
        if let Some(fun) = opt.as_ref() {
            let this = JsValue::NULL;
            let won: JsValue = won.into();
            fun.call1(&this, &won)
                .expect("failed to report game result to JS");
            *opt = None;
        }
    });
}

fn finished(game_state: GameState) -> bool {
    match game_state {
        GameState::Won | GameState::Lost => true,
        _ => false,
    }
}

fn reveal_all_mines(game: &GameHandle) {
    let width = game.get_width();
    let height = game.get_height();
    let window = web_sys::window().expect("no global `window` exists");
    let document = window.document().expect("should have a document on window");

    for y in 0..height {
        for x in 0..width {
            let i = y * width + x;
            if let Some(mine_field) = game.get_board() {
                if mine_field.get_tile(x, y).is_mine() {
                    if let Some(tile) = document.get_element_by_id(&format!("{}", i)) {
                        tile.set_inner_html(MINE);
                        tile.set_attribute("uncovered", "true").unwrap();
                    }
                }
            }
        }
    }
}

fn set_state_icon(state: &str, document: &Document) {
    if let Some(button) = document.get_element_by_id("startButton") {
        button.set_inner_html(state);
    }
}

fn update_counter(game: &GameHandle, document: &Document) {
    if let Some(board) = game.get_board() {
        if let Some(counter) = document.get_element_by_id("counter") {
            counter.set_inner_html(&format_mine_count(board.get_unmarked_mine_count()));
        }
    }
}

fn format_mine_count(count: usize) -> String {
    format!("{:0>3}", count)
}
