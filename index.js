import './style.css';

const rust = import('./pkg/wasminesweeper');

var start = 0;
var timer;

if ('serviceWorker' in navigator) {
    window.addEventListener('load', () => {
        navigator.serviceWorker.register('/service-worker.js').then(registration => {
            console.log('SW registered: ', registration);
        }).catch(registrationError => {
            console.log('SW registration failed: ', registrationError);
        });
    });
} else {
    console.error("ServiceWorker not available!");
}

function game_started() {
    startTimer();
}

function game_ended(won) {

    stopTimer();

    let now = Date.now();
    let duration = now - start;

    updateClock(duration);

    if (won) {
        // TODO register high score
    }
}

function updateClock(duration) {
    let seconds = Math.round(duration / 1000);
    document.getElementById("clock").innerHTML = pad(seconds);
}

function pad(seconds) {
    return seconds <= 999 ? `00${seconds}`.slice(-3) : seconds;
}

function startTimer() {
    start = Date.now();
    timer = setInterval(() => updateClock(Date.now() - start), 1000);
}

function stopTimer() {
    if (timer) {
        clearInterval(timer);
    }
}

function reset(wasm) {
    document.getElementById("clock").innerHTML = "000";
    wasm.start_minesweeper(game_started, game_ended);
}

rust.then(wasm => {
    wasm.init_panic_hook();
    document.getElementById("main").addEventListener("contextmenu", e => e.preventDefault());
    document.getElementById('startButton').addEventListener("click", e => reset(wasm));
    reset(wasm);
}).catch(console.error);